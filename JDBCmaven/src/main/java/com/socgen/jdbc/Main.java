package com.socgen.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection dbCon = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/socGenData?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
					"root", "");

			String qryFetch1 = "SELECT * FROM `userdetails1`";
			java.sql.Statement stmt = dbCon.createStatement();
			ResultSet rs = stmt.executeQuery(qryFetch1);

			while (rs.next()) {
				System.out.print("ID:  " + rs.getInt("userID"));
				System.out.print("Name:  " + rs.getString("userName"));
				System.out.println("Address:  " + rs.getString("userAddress"));
			}
			rs.close();

			// Insert
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter your name");
			String name = sc.nextLine();
			System.out.println("Enter your address");
			String address = sc.nextLine();

			String insertQry = "insert into userdetails1(userName,userAddress) values ('" + name + "','" + address
					+ "')";

			if (stmt.executeUpdate(insertQry) > 0) {
				System.out.println("Account created successfully");
			} else {
				System.out.println("Please try again");
			}

			String qryFetch2 = "SELECT * FROM `userdetails1`";
			ResultSet rs1 = stmt.executeQuery(qryFetch2);
			while (rs1.next()) {
				System.out.print("ID:  " + rs1.getInt("userID"));
				System.out.print("Name:  " + rs1.getString("userName"));
				System.out.println("Address:  " + rs1.getString("userAddress"));
			}
			rs1.close();

			// Delete
			System.out.println("Enter the name to be deleted");
			String dname = sc.nextLine();

			String deleteQry = "delete from userdetails1 where userName='" + dname + "'";
			stmt.executeUpdate(deleteQry);

			String qryFetch3 = "SELECT * FROM `userdetails1`";
			ResultSet rs2 = stmt.executeQuery(qryFetch3);
			while (rs2.next()) {
				System.out.print("ID:  " + rs2.getInt("userID"));
				System.out.print("Name:  " + rs2.getString("userName"));
				System.out.println("Address:  " + rs2.getString("userAddress"));
			}
			rs2.close();

			// Update
			System.out.println("Enter the name to be update");
			String uname = sc.nextLine();
			System.out.println("Enter the userID");
			int id = sc.nextInt();

			String updateQry = "update userDetails1 set userName='" + uname + "' where userID='" + id + "'";
			stmt.executeUpdate(updateQry);

			String againq = "select * from userdetails1";
			ResultSet rs4 = stmt.executeQuery(againq);
			while (rs4.next()) {
				System.out.print("ID: " + rs4.getInt("userID"));
				System.out.print("Name: " + rs4.getString("userName"));
				System.out.println("Address: " + rs4.getString("userAddress"));
			}

			rs4.close();
			dbCon.close();

		} catch (ClassNotFoundException e) {
			System.out.println("Driver not configured..." + e.getMessage());
		} catch (SQLException e) {
			System.out.println("Unable to connect to db: socgendata" + e.getMessage());
		}

	}
}
